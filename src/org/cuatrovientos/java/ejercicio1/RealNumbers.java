package org.cuatrovientos.java.ejercicio1;

import java.util.Random;

public class RealNumbers implements Statistics {

	private double[] values;

	public RealNumbers() {
		this.values = new double[5];
		this.initialize();
	}

	private void initialize() {
		Random rnd = new Random();
		for (int i = 0; i < this.values.length; i++) {
			this.values[i] = rnd.nextDouble(100) * 100;
		}
	}

	@Override
	public double min() {
		double min = this.values[0];
		for (int i = 0; i < this.values.length; i++) {
			if (min > this.values[i])
				min = this.values[i];
		}
		return min;
	}

	@Override
	public double max() {
		double max = this.values[0];
		for (int i = 0; i < this.values.length; i++) {
			if (max < this.values[i])
				max = this.values[i];
		}
		return max;
	}

	@Override
	public double sum() {
		double suma = 0;
		for (int i = 0; i < this.values.length; i++) {
			suma = suma + this.values[i];
		}
		return suma;
	}

	@Override
	public void print() {
		System.out.println("El contenido del array es: ");
		for (int i = 0; i < this.values.length; i++) {
			System.out.println("Elemento posicion " + i + " : " + this.values[i]);
		}
		System.out.println("El minimo es: " + this.min());
		System.out.println("El maximo es: " + this.max());
		System.out.println("La suma es: " + this.sum());

	}

}
