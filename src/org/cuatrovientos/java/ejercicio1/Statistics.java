package org.cuatrovientos.java.ejercicio1;

public interface Statistics {

	public double min();
	public double  max();
	public double sum();
	public void print();
	
}
