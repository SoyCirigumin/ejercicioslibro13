package org.cuatrovientos.java.app;

import org.cuatrovientos.java.ejercicio1.RealNumbers;
import org.cuatrovientos.java.ejercicio2.Car;
import org.cuatrovientos.java.ejercicio2.Motorbike;
import org.cuatrovientos.java.ejercicio3.Circulo;
import org.cuatrovientos.java.ejercicio3.Cuadrado;
import org.cuatrovientos.java.ejercicio4.FileAgenda;
import org.cuatrovientos.java.ejercicio4.MemoryAgenda;
import org.cuatrovientos.java.ejercicio4.StringAgenda;

public class App {

	public App() {

	}

	public static void main(String[] args) {

		// Ejercicio 1:
		System.out.println("Ejercicio 1:");
		RealNumbers values = new RealNumbers();
		values.print();
		// Ejercicio 2:
		System.out.println("Ejercicio 2:");
		Motorbike moto = new Motorbike();
		Car coche = new Car();
		System.out.println(moto.accelerate(40));
		System.out.println(moto.brake(20));
		System.out.println(moto.accelerate(140));
		System.out.println(moto.brake(120));
		System.out.println(coche.accelerate(40));
		System.out.println(coche.brake(20));
		System.out.println(coche.accelerate(140));
		System.out.println(coche.brake(120));
		// Ejercicio 3:
		System.out.println("Ejercicio 3:");
		Cuadrado cuadrado = new Cuadrado(5.3F);
		System.out.println("El area del cuadrado es " + cuadrado.area());
		cuadrado.dibujar();
		cuadrado.rotar();
		Circulo circulo = new Circulo(2.3F);
		System.out.println("El area del circulo es " + circulo.area());
		circulo.dibujar();
		// Ejercicio 4:
		System.out.println("Ejercicio 4:");
		MemoryAgenda memory = new MemoryAgenda();
		memory.showContacts();
		StringAgenda string = new StringAgenda();
		string.showContacts();
		FileAgenda file = new FileAgenda();
		file.showContacts1();
		file.showContact2();
	}

}
