package org.cuatrovientos.java.ejercicio2;

public interface Vehicle {

	public static final int MAX_SPEED = 120;

	public String brake(int i);

	public String accelerate(int i);

	public int possitions();

}
