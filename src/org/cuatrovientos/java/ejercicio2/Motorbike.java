package org.cuatrovientos.java.ejercicio2;

public class Motorbike implements Vehicle {

	private int speed;
	private static final int POSITIONS = 2;

	public Motorbike() {
		this.speed=0;
	}

	public int getPositions() {
		return POSITIONS;
	}

	@Override
	public String brake(int i) {
		int aux = this.speed;
		if (aux - i <= 0) {
			this.speed = 0;
			return ("Estamos parados en la moto");
		} else {
			this.speed = this.speed - i;
			return ("Frenamos la moto de " + aux + " a " + this.speed);
		}

	}

	@Override
	public String accelerate(int i) {
		int aux = this.speed;
		if (aux + i > Vehicle.MAX_SPEED) {
			this.speed = Vehicle.MAX_SPEED;
			return ("la moto va al maximo permitido");
		} else {
			this.speed = this.speed + i;
			return ("Aceleramos la moto " + aux + " a " + this.speed);
		}
	}

	@Override
	public int possitions() {
		return this.getPositions();
	}

}
