package org.cuatrovientos.java.ejercicio2;

public class Car implements Vehicle {

	private int speed;
	private static final int POSITIONS = 4;

	public Car() {
		this.speed=0;
	}
	public int getPositions() {
		return POSITIONS;
	}

	@Override
	public String brake(int i) {
		int aux = this.speed;
		if (aux - i <= 0) {
			this.speed = 0;
			return ("Estamos parados en el coche");
		} else {
			this.speed = this.speed - i;
			return ("Frenamos el coche de " + aux + " a " + this.speed);
		}

	}

	@Override
	public String accelerate(int i) {
		int aux = this.speed;
		if (aux + i > Vehicle.MAX_SPEED) {
			this.speed = Vehicle.MAX_SPEED;
			return ("El coche va al maximo permitido");
		} else {
			this.speed = this.speed + i;
			return ("Aceleramos el coche " + aux + " a " + this.speed);
		}
	}

	@Override
	public int possitions() {
		return this.getPositions();
	}

}
