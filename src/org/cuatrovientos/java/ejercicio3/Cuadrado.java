package org.cuatrovientos.java.ejercicio3;

public class Cuadrado extends Figura implements Dibujable, Rotable {

	private double lado;

	public Cuadrado(double lado) {
		this.lado = lado;
	}

	@Override
	public void rotar() {
		System.out.println("Funcion rotar -> Soy un cuadrado y mi lado es: " + this.lado);

	}

	@Override
	public void dibujar() {
		System.out.println("Funcion dibujar -> Soy un cuadrado y mi lado es: " + this.lado);
	}

	@Override
	public double area() {
		return this.lado * this.lado;
	}

}
