package org.cuatrovientos.java.ejercicio3;

public abstract class Figura {

	public abstract double area();
}
