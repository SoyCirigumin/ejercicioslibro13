package org.cuatrovientos.java.ejercicio3;

public class Circulo extends Figura implements Dibujable {

	private double radio;

	public Circulo(double radio) {
		this.radio = radio;
	}

	@Override
	public void dibujar() {
		System.out.println("Funcion dibujar -> Soy un circulo y mi radio es: " + this.radio);
	}

	@Override
	public double area() {
		return Math.PI * Math.pow(this.radio, 2);
	}

}
