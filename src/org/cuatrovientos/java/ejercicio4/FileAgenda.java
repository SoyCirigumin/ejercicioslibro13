package org.cuatrovientos.java.ejercicio4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class FileAgenda {

	public void showContacts1() {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			// Apertura del fichero y creacion de BufferedReader para poder
			// hacer una lectura comoda (disponer del metodo readLine()).
			archivo = new File(
					"/home/cirigumin/Escritorio/EclipseProyects/EjerciciosLibro13/src/org/cuatrovientos/java/ejercicio4/contactos.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			// Lectura del fichero
			String linea;
			int i = 0;
			System.out.println("Metodo showContacts1() de la clase FileAgenda: ");
			while ((linea = br.readLine()) != null) {
				System.out.println("Contacto de la posicion " + i + " " + linea);
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// En el finally cerramos el fichero, para asegurarnos
			// que se cierra tanto si todo va bien como si salta
			// una excepcion.
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public void showContact2() {
		File f = new File(
				"/home/cirigumin/Escritorio/EclipseProyects/EjerciciosLibro13/src/org/cuatrovientos/java/ejercicio4/contactos.txt");
		Scanner s;
		try {
			System.out.println("Metodo showContacts2() de la clase FileAgenda: ");
			s = new Scanner(f);
			int i = 0;
			while (s.hasNextLine()) {
				String linea = s.nextLine();
				System.out.println("Contacto de la posicion " + i + " " + linea);
				i++;
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
