package org.cuatrovientos.java.ejercicio4;

public class MemoryAgenda implements Agenda {

	protected String[] contacts = { "Juan 91223333", "Luis 4444445", "Ana 45622222", "Javi 236777", "Cris 1990263" };

	public MemoryAgenda() {

	}

	@Override
	public void showContacts() {
		System.out.println("Metodo showContacts() de la clase MemoryAgenda: ");
		for (int i = 0; i < contacts.length; i++) {
			System.out.println("Contacto de la posicion " + i + " " + contacts[i]);
		}

	}

}
