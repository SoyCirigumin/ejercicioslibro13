package org.cuatrovientos.java.ejercicio4;

import java.util.Scanner;

public class StringAgenda implements Agenda {
	private static Scanner sc;

	public StringAgenda() {

	}

	@Override
	public void showContacts() {
		sc = new Scanner(System.in);
		String cadena = "";
		System.out.println("Introduzca un contacto");
		cadena = sc.nextLine();
		System.out.println("Metodo showContacts() de la clase StringAgenda: " + cadena);
	}

}
